
import './App.css';
import Countdown from './Components/Countdown/Countdown';
import DigitalClock from './Components/DigitalClock/DigitalClock';
import Stopwatch from './Components/Stopwatch/Stopwatch'

function App() {
  return (
    <div className="App">
     <DigitalClock></DigitalClock>
     <Countdown></Countdown>
    <Stopwatch></Stopwatch>
    </div>
  );
}

export default App;
